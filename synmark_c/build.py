#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import PerfTester
from options import Options


class SynmarkTimeout:
    def __init__(self):
        self._options = Options()
    def GetDuration(self):
        if self._options.type == "daily":
            return 120
        return 30

def iterations(bench, hw):
    if bench == "synmark.OglTexFilterTri":
        if hw.startswith("gen8"):
            return 10
    if bench == "synmark.OglTerrainPanTess":
        if hw.startswith("gen8"):
            return 5
    

low_variance_benchmarks = ["synmark.OglTerrainPanTess",
                           "synmark.OglPSPhong",
                           "synmark.OglPSPom",
                           "synmark.OglTexMem512",
                           "synmark.OglTexFilterTri",
                           "synmark.OglShMapPcf"]

build(PerfTester(low_variance_benchmarks, iterations=3),
      time_limit=SynmarkTimeout())
