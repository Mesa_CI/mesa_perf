#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import PerfTester
from options import Options


class SynmarkTimeout:
    def __init__(self):
        self._options = Options()
    def GetDuration(self):
        if self._options.type == "daily":
            return 120
        return 30

def iterations(bench, hw):
    if bench == "synmark.OglHdrBloom":
        if hw.startswith("gen9"):
            return 4


high_variance_benchmarks = ["synmark.OglFillTexSingle",
                            "synmark.OglGeomPoint",
                            "synmark.OglHdrBloom",
                            "synmark.OglCSCloth",
                            "synmark.OglShMapVsm",
                            "synmark.OglFillTexMulti"]

build(PerfTester(high_variance_benchmarks, iterations=2,
                  custom_iterations_fn=iterations),
      time_limit=SynmarkTimeout())
