#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2014-2021.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/


import argparse
import sys
import os
import time
import stat
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from utils.command import rmtree


# from http://stackoverflow.com/questions/6879364/print-file-age-in-seconds-using-python
def file_age_in_seconds(pathname):
    return time.time() - os.stat(pathname)[stat.ST_MTIME]


def file_age_in_days(pathname):
    return file_age_in_seconds(pathname) / (60*60*24)


def main():
    parser = argparse.ArgumentParser(description=("Script to clean results "
                                                  "dir of old results"))
    parser.add_argument('-d', '--days', type=int, default=50,
                        help=("Max age (in days) to preserve " +
                              "(default %(default)i days)"))
    parser.add_argument('-r', '--result-path', type=str,
                        default="/mnt/jenkins/results",
                        help=('Path to results (default: %(default)s)'))
    opts = parser.parse_args()

    result_path = opts.result_path + '/'

    for a_dir in os.listdir(result_path):
        if a_dir == "traceValidator":
            continue
        if a_dir == "perf_win":
            continue
        sub_dir = result_path + a_dir
        for a_build_dir in os.listdir(sub_dir):
            build_dir = sub_dir + "/" + a_build_dir
            if os.path.islink(build_dir):
                continue
            if file_age_in_days(build_dir) > opts.days:
                rmtree(build_dir)


if __name__ == "__main__":
    main()
