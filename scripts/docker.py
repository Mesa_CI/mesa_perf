#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2020.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import argparse
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from docker_runner import DockerRunner


def main():
    parser = argparse.ArgumentParser(description=("Script to run a given " +
                                                  "docker tag with CI " +
                                                  "env/configuration"))
    parser.add_argument('-t', '--tag', type=str, required=True,
                        help="Docker tag to run")
    parser.add_argument('-r', '--rm', action='store_true',
                        help=('Remove the container after finishing ' +
                              '(default: false)'))
    opts = parser.parse_args()

    drunner = DockerRunner(opts.tag, interactive=True,
                           build_root='/tmp/build_root/m64', network=True)
    drunner.run('/bin/bash', env=os.environ, rm=opts.rm)

    if opts.rm:
        print("NOTE: Container was *REMOVED*, re-run without --rm if this was "
              "not intended!")


if __name__ == "__main__":
    main()
