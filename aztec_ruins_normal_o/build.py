#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import PerfTester
from options import Options

hw = Options().hardware
driver = hw.split('_')[-1]

if driver == "vk":
    aztec_test = "gfxbench5.aztec_ruins_vk_normal_o"
else:
    aztec_test = "gfxbench5.aztec_ruins_gl_normal_o"

# Note: fullscreen + vulkan currently crashes, so all benchmarking should be
# done in windowed mode
build(PerfTester(aztec_test, iterations=3, windowed=True))
