#!/usr/bin/env python3
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from docker_runner import get_docker_tag
from project_map import ProjectMap
from testers import AbnPerfTester


docker_tag = get_docker_tag(ProjectMap().current_project())
# TODO: add support for overriding tag from environment
if not docker_tag:
    raise RuntimeError("Docker tag needed to run this, check build_spec!")

# Note: discard first iteration since shaders are being compiled there
build(AbnPerfTester('Team Fortress 2', iterations=4, windowed=False,
                    discard=1, docker_tag=docker_tag))
